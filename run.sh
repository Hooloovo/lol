#!/bin/bash

# Проверяем наличие Node.js
if ! command -v node &> /dev/null
then
    echo "Node.js не установлен. Пожалуйста, установите его и попробуйте снова."
    exit 1
fi

# Проверяем наличие npm
if ! command -v npm &> /dev/null
then
    echo "npm не установлен. Пожалуйста, установите его и попробуйте снова."
    exit 1
fi

# Функция для проверки установленных зависимостей
check_dependencies() {
    if [ ! -d "node_modules" ]; then
        return 1
    fi

    if ! npm list playwright &> /dev/null; then
        return 1
    fi

    if ! npm list fs &> /dev/null; then
        return 1
    fi

    return 0
}

# Проверка и установка зависимостей
if ! check_dependencies; then
    echo "Установка зависимостей..."
    npm install playwright fs
    echo "Установка браузеров для Playwright..."
    npx playwright install
else
    echo "Все зависимости уже установлены."
fi

# Запуск скрипта
echo "Запуск скрипта index.js..."
node index.js