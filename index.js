const fs = require('fs');
const { chromium } = require('playwright');

const fetchHtml = async (url) => {
  console.log('Запускаем браузер...');
  // Запускаем браузер
  const browser = await chromium.launch();
  console.log('Браузер создан');

  console.log('Создаем страницу...');
  // Создаем новую страницу
  const page = await browser.newPage();
  console.log('Страница создана');

  // console.log('Устанавливаем необходимые заголовки...');
  // // Устанавливаем необходимые заголовки
  // await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3');
  // console.log('Заголовки установлены');

  console.log('Переходим на страницу...');
  // Переходим на страницу
  await page.goto(url, { waitUntil: 'networkidle' });
  console.log('Страница открыта');

  console.log('Получаем полный HTML контент страницы...');
  // Получаем полный HTML контент страницы
  const html = await page.content();
  console.log('HTML контент получен');

  console.log('Закрываем браузер...');
  // Закрываем браузер
  await browser.close();
  console.log('Браузер закрыт');
  
  return html;
};

// Заданный класс
const regex = /(\+|-)[0-9]{1,2} LP/g;

// Функция для чтения URL из файла
function readUrlsFromFile(filePath) {
  return fs.readFileSync(filePath, 'utf-8').split('\n').filter(Boolean);
}

// Функция для получения HTML кода страницы по URL
async function fetchPageContent(url) {
  try {
    const response = await fetchHtml(url);

    fs.writeFileSync('downloaded.html', response, 'utf-8');

    return response;
  } catch (error) {
    console.error(`Error fetching URL: ${url} - ${error.message}`);
    return null;
  }
}

// Основная функция
(async function() {
  const inputFilePath = 'urls.txt';
  const outputFilePath = 'result.txt';
  
  // Чтение URL-ов из файла
  const urls = readUrlsFromFile(inputFilePath);
  let allExtractedHtml = [];

  for (const url of urls) {
    const html = await fetchPageContent(url);
    if (html) {
      allExtractedHtml = [...html.matchAll(regex)].map(match => match[0]);
    }
  }

  // Запись результата в файл
  fs.writeFileSync(outputFilePath, allExtractedHtml.join('\n'), 'utf-8');
  console.log(`Results saved to ${outputFilePath}`);
})();